<?php

declare(strict_types=1);

namespace ElektroPotkan\Scheduler\Schedulers;

use DateTimeInterface;
use Nette\IOException;
use Nette\Utils\FileSystem;

use ElektroPotkan\Scheduler\IJob;


class Locking extends Simple {
	/** @var string */
	protected $path;
	
	
	public function __construct(string $path){
		$this->path = $path;
	} // constructor
	
	public function run(): void {
		if(!file_exists($this->path) && !mkdir($this->path, 0777, true) && !is_dir($this->path)){
			throw new IOException(sprintf('Directory `%s` was not created', $this->path));
		};
		
		parent::run();
	} // run
	
	protected function runJob(string $id, IJob $job, DateTimeInterface $now): void {
		$lockFile = $this->path.'/'.md5($id).'.lock';
		
		// acquire an exclusive lock
		$lock = fopen($lockFile, 'w+');
		if($lock === false){
			throw new IOException('Unable to open lock-file!');
		}
		elseif(!flock($lock, LOCK_EX | LOCK_NB)){
			fclose($lock);
			return;
		};
		
		try {
			parent::runJob($id, $job, $now);
		}
		finally {
			// unlock
			flock($lock, LOCK_UN);
			fclose($lock);
			
			try {
				FileSystem::delete($lockFile);
			}
			catch(IOException $e){};
		};
	} // runJob
} // class Locking
