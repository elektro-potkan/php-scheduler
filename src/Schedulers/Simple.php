<?php

declare(strict_types=1);

namespace ElektroPotkan\Scheduler\Schedulers;

use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Nette;
use Nette\Utils\DateTime;
use Throwable;

use ElektroPotkan\Scheduler\IJob;
use ElektroPotkan\Scheduler\ILogger;
use ElektroPotkan\Scheduler\IScheduler;


class Simple implements IScheduler {
	use Nette\SmartObject;
	
	
	/** @var IJob[] */
	protected $jobs = [];
	
	/** @var ?ILogger */
	protected $logger = null;
	
	
	public function run(): void {
		$now = new DateTimeImmutable;
		
		$this->log('Scheduler: Running (' . $this->formatLogTime($now) . ')...');
		$this->runJobs($now);
		$this->log('Scheduler: Finished run.');
	} // run
	
	protected function runJobs(
		DateTimeInterface $now,
		?DateTimeInterface $lastCheck = null
	): void {
		if($lastCheck !== null){
			$this->log('Scheduler: Running (' . $this->formatLogTime($now) . ', last check ' . $this->formatLogTime($lastCheck) . ')...');
		};
		
		foreach($this->jobs as $id => $job){
			if(!$this->isJobDue((string) $id, $job, $now, $lastCheck)){
				continue;
			};
			
			$this->runJob((string) $id, $job, $now);
		};
	} // runJobs
	
	/**
	 * Calls job's isDue method (to be overriden by descendants)
	 */
	protected function isJobDue(
		string $id,
		IJob $job,
		DateTimeInterface $now,
		?DateTimeInterface $lastCheck
	): bool {
		return $job->isDue($now, $lastCheck);
	} // isJobDue
	
	/**
	 * Runs given job
	 */
	protected function runJob(string $id, IJob $job, DateTimeInterface $now): void {
		$this->log("Scheduler: Running job '$id'...");
		
		$this->runJobBefore($id, $job, $now);
		
		try {
			$job->run();
		}
		catch(Throwable $e){
			$this->log($e, ILogger::EXCEPTION);
		};
		
		$this->runJobAfter($id, $job, $now);
		
		$this->log("Scheduler: Finished job '$id'.");
	} // runJob
	
	/**
	 * Called before given job is run (to be overriden by descendants)
	 */
	protected function runJobBefore(string $id, IJob $job, DateTimeInterface $now): void {}
	
	/**
	 * Called after given job is run (to be overriden by descendants)
	 */
	protected function runJobAfter(string $id, IJob $job, DateTimeInterface $now): void {}
	
	/**
	 * @param string|int|null $key
	 */
	public function add(IJob $job, $key = null): void {
		if($key !== null){
			$this->jobs[$key] = $job;
			return;
		};
		
		$this->jobs[] = $job;
	} // add
	
	/**
	 * @param string|int $key
	 */
	public function get($key): ?IJob {
		return $this->jobs[$key] ?? null;
	} // get
	
	/**
	 * @return IJob[]
	 */
	public function getAll(): array {
		return $this->jobs;
	} // getAll
	
	/**
	 * @param string|int $key
	 */
	public function remove($key): void {
		unset($this->jobs[$key]);
	} // remove
	
	public function removeAll(): void {
		$this->jobs = [];
	} // removeAll
	
	public function setLogger(ILogger $logger): void {
		$this->logger = $logger;
	} // setLogger
	
	/**
	 * @param mixed $value
	 */
	protected function log($value, string $level = ILogger::INFO): void {
		if($this->logger !== null){
			$this->logger->log($value, $level);
		};
	} // log
	
	protected function formatLogTime(DateTimeInterface $time): string {
		$time = DateTime::from($time);
		$time->setTimezone(new DateTimeZone('UTC'));
		return $time->format('Y-m-d H:i:s') . ' UTC';
	} // formatLogTime
} // class Simple
