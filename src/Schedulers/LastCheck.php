<?php

declare(strict_types=1);

namespace ElektroPotkan\Scheduler\Schedulers;

use DateTimeImmutable;
use DateTimeInterface;
use Nette\Utils\SafeStream;


class LastCheck extends Locking {
	protected function runJobs(
		DateTimeInterface $now,
		?DateTimeInterface $lastCheck = null
	): void {
		$lastCheck = $this->loadTime($this->buildLastCheckFilePath());
		parent::runJobs($now, $lastCheck);
		$this->saveTime($this->buildLastCheckFilePath(), $now);
	} // runJobs
	
	protected function buildLastCheckFilePath(): string {
		return SafeStream::PROTOCOL . '://' . $this->path . '/last-check';
	} // buildLastCheckFilePath
	
	protected function loadTime(string $file): ?DateTimeImmutable {
		if(file_exists($file)){
			$time = file_get_contents($file);
			if($time !== false){
				$time = DateTimeImmutable::createFromFormat('U', $time);
				if($time !== false){
					return $time;
				};
			};
		};
		
		return null;
	} // loadTime
	
	protected function saveTime(string $file, DateTimeInterface $time): void {
		file_put_contents($file, $time->format('U'));
	} // saveTime
} // class LastCheck
