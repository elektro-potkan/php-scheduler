<?php

declare(strict_types=1);

namespace ElektroPotkan\Scheduler\Schedulers;

use DateTimeInterface;
use Nette\Utils\SafeStream;

use ElektroPotkan\Scheduler\IJob;


class LastRun extends LastCheck {
	protected function isJobDue(
		string $id,
		IJob $job,
		DateTimeInterface $now,
		?DateTimeInterface $lastCheck
	): bool {
		$lastRun = $this->loadTime($this->buildJobLastRunFilePath($id, $job));
		return $job->isDue($now, $lastCheck, $lastRun);
	} // isJobDue
	
	protected function runJobAfter(string $id, IJob $job, DateTimeInterface $now): void {
		$this->saveTime($this->buildJobLastRunFilePath($id, $job), $now);
	} // runJobAfter
	
	protected function buildJobLastRunFilePath(string $id, IJob $job): string {
		return SafeStream::PROTOCOL . '://' . $this->path . '/' . md5($id . get_class($job)) . '.last-run';
	} // buildJobLastRunFilePath
} // class LastRun
