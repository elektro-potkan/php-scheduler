<?php

declare(strict_types=1);

namespace ElektroPotkan\Scheduler;


interface IScheduler {
	function run(): void;
	
	/**
	 * @param string|int|null $key
	 */
	function add(IJob $job, $key = null): void;
	
	/**
	 * @param string|int $key
	 */
	function get($key): ?IJob;
	
	/**
	 * @return IJob[]
	 */
	function getAll(): array;
	
	/**
	 * @param string|int $key
	 */
	function remove($key): void;
	
	function removeAll(): void;
} // interface IScheduler
