<?php

declare(strict_types=1);

namespace ElektroPotkan\Scheduler\Jobs;


class Callback extends AExpression {
	/** @var callable */
	private $callback;
	
	
	public function __construct(string $cron, callable $callback){
		parent::__construct($cron);
		$this->callback = $callback;
	} // constructor
	
	public function run(): void {
		call_user_func($this->callback);
	} // run
	
	public function getCallback(): callable {
		return $this->callback;
	} // getCallback
} // class Callback
