<?php

declare(strict_types=1);

namespace ElektroPotkan\Scheduler\Jobs;

use Cron\CronExpression;
use DateTimeInterface;
use Nette;

use ElektroPotkan\Scheduler\IJob;


abstract class AExpression implements IJob {
	use Nette\SmartObject;
	
	
	/** @var CronExpression */
	protected $expression;
	
	
	public function __construct(string $cron){
		$this->expression = new CronExpression($cron);
	} // constructor
	
	public function isDue(
		DateTimeInterface $now,
		?DateTimeInterface $lastCheck = null,
		?DateTimeInterface $lastRun = null
	): bool {
		return (
			$this->expression->isDue($now)
			|| ($lastCheck !== null && $lastCheck < $this->expression->getPreviousRunDate($now))
		);
	} // isDue
	
	public function getExpression(): CronExpression {
		return $this->expression;
	} // getExpression
} // class AExpression
