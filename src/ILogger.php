<?php

declare(strict_types=1);

namespace ElektroPotkan\Scheduler;


interface ILogger {
	const
		DEBUG = 'debug',
		INFO = 'info',
		WARNING = 'warning',
		ERROR = 'error',
		EXCEPTION = 'exception',
		CRITICAL = 'critical';
	
	/**
	 * @param mixed $value
	 */
	function log($value, string $level = self::INFO): void;
} // interface ILogger
