<?php

declare(strict_types=1);

namespace ElektroPotkan\Scheduler;

use DateTimeInterface;


interface IJob {
	/**
	 * Returns whether the job should be run
	 * @param ?DateTimeInterface $lastCheck - time of previous call of this method
	 *   null if scheduler runs for the first time or if it does not support this feature
	 * @param ?DateTimeInterface $lastRun - time of previous job run
	 *   null if the job did not run before or if scheduler does not support this feature
	 * @return bool - true if job should be run now
	 */
	function isDue(
		DateTimeInterface $now,
		?DateTimeInterface $lastCheck = null,
		?DateTimeInterface $lastRun = null
	): bool;
	
	function run(): void;
} // interface IJob
